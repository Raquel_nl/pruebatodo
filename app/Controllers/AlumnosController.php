<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\GruposModel;
use App\Models\AlumnosModel;

class AlumnosController extends BaseController
{
    public function index()
    {
        //Llamamos a helper_edad
        helper('edad');

        //Nueva Instancia de los datos recogidos en ALumnosModel
        $alumnos = new AlumnosModel();
        
        //Recogemos del modelo anterior todos los alumnos y lo ordenamos by default *Solo lo ponemos por visibilidad
        $data['alumnos'] = $alumnos->orderby('id','DESC')->findAll();
        

        //Indicamos que se muestre dicha dirección y $data: Sabiendo que está compuesta por todo lo anterior mencionado
        return view('alumnos/viewAlumnos', $data);
        
    }
}