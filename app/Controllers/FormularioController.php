<?php

namespace App\Controllers;

use App\Models\FormularioModel;

class FormularioController extends BaseController {

    public function index() {
        helper('form');
        $alumnos = new FormularioController();
        echo view('alumnos/FormularioView');
    }

    public function insertar() {

        $model = new FormularioModel();
        $alumno = [
            "NIA" => $this->request->getPost('nia'),
            "nombre" => $this->request->getPost('nombre'),
            "apellido1" => $this->request->getPost('apellido1'),
            "apellido2" => $this->request->getPost('apellido2'),
            "fecha_nac" => $this->request->getPost('nacimiento'),
            "nif" => $this->request->getPost('nif'),
            "email" => $this->request->getPost('email'),
            "repito" => $this->request->getPost('repito')===null?'No':'Sí',
            "Nivel_ingles" => $this->request->getPost('Nivel_ingles'),
        ];

        //$model->insert($alumno);
        print_r($alumno);
        echo "Acabo de insertar al alumno, ve a la tabla, actualiza y compruébalo";
    }

}
