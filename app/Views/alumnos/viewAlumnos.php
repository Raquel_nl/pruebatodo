
<!-- <body> -->
<div class="container">
    <table class="table caption-top" style="margin: 0 auto !important">

        <caption>
        </caption>

        <thead>
            <tr>
                <th> Foto </th>
                <th> id </th>
                <th> NIA</th>
                <th> Nombre </th>
                <th> Apellido1 </th>
                <th> Apellido2 </th>
                <th> Fecha_Nac </th>
                <th> NIF </th>
                <th> Email </th>
                <th>
                    <a class="btn btn-light btn-outline-danger" href="<?= site_url('formularioController/formCreate') ?>">Add</a>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($alumnos as $alumno) : ?>
                <?php
                $datetime1 = date_create();
                $datetime2 = date_create($alumno['fecha_nac']);
                $interval = date_diff($datetime1, $datetime2);
                ?>

                <tr class=" <?= $interval->format('%y') > 30 ? 'text-danger' : 'text' ?> ">
                    <td> <img style="width: 50px" src="<?= base_url('imagenes/' . sprintf('%06s', $alumno['id'])) ?>.jpg"> </td>
                    <td> <b> <?= $alumno["id"] ?> </b>
                    <td> <?= $alumno["NIA"] ?> </td>
                    <td> <b> <?= $alumno["nombre"] ?> </b> </td>
                    <td> <?= $alumno["apellido1"] ?> </td>
                    <td> <?= $alumno["apellido2"] ?> </td>
                    <td> <?= $alumno["fecha_nac"] ?> : <?= $interval->format('%y') ?> </td>
                    <td> <?= $alumno["nif"] ?> </td>
                    <td> <?= $alumno["email"] ?> </td>
                    <td>
                        <div class="d-grid gap-2 d-md-flex justify-content-md-end">                                                     
                            <a class="btn btn-light btn-outline-danger" href="<?= site_url('formularioController/formEdit/'.$alumno['id']) ?>">Edit</a>                            
                        </div>
                    </td>
                </tr>

            <?php
            endforeach; ?>
        </tbody>

    </table>
</div>
</body>