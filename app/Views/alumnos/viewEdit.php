
<!-- <body> -->
<div class="container px-5 pt-5">

    <form method="post" action="<?= site_url('formularioController/formUpdate/'.$alumno['id']) ?>">
        <!-- method="post Porque queremos recoger los datos para hacer UPDATE en el metodo del Controller" -->
        <!-- action="..." Se debe de poner esta URL para que se coja el alumno que quisimos editar desde viewAlumnos -->
        <div class="mb-3">
            <label for="NIA" class="form-label "><b>NIA</b></label>
            <input type="text" name="NIA" size="8" value="<?= $alumno['NIA'] ?>" class="form-control" id="NIA" placeholder="NIA">
        </div>
            
        <!-- <input type="hidden" name="id" value="<?= $alumno['id'] ?>"> -->
        <!-- ¿Si se quisiese actualizar el id se podria realizar de esta manera? -->

        <div class="mb-3">
            <label for="nombre" class="form-label"><b>Nombre</b></label>
            <input type="text" name="nombre" value="<?= $alumno['nombre'] ?>" class="form-control" id="nombre" placeholder="Nombre">
        </div>

        <div class="mb-3">
            <label for="apellido1" class="form-label"><b>Apellido1</b></label>
            <input type="text" name="apellido1" value="<?= $alumno['apellido1'] ?>" class="form-control" id="apellido1" placeholder="Apellido1">
        </div>

        <div class="mb-3">
            <label for="apellido2" class="form-label"><b>Apellido2</b></label>
            <input type="text" name="apellido2" value="<?= $alumno['apellido2'] ?>" class="form-control" id="apellido2" placeholder="Apellido2">
        </div>

        <div class="mb-3">
            <label for="fecha_nac" class="form-label"><b>Fecha Nacimiento</b></label>
            <input type="text" name="fecha_nac" value="<?= $alumno['fecha_nac'] ?>" class="form-control" id="fecha_nac" placeholder="Fecha Nacimiento" >
        </div>

        <div class="mb-3">
            <label for="nif" class="form-label"><b>NIF</b></label>
            <input type="text" name="nif" size="9" value="<?= $alumno['nif'] ?>" class="form-control" id="nif" placeholder="NIF">
        </div>

        <div class="mb-3 mt-3">
            <label for="email" class="form-label"><b>Email:</b></label>
            <input type="email" name="email" value="<?= $alumno['email'] ?>" class="form-control" id="email" placeholder="Enter email">
        </div>

        <button name="submit" type="submit" class="btn btn-light btn-outline-danger">Submit Edit</button>
    </form>
    <?php
        echo '<br>';
        print_r($alumno);
    ?>
</div>


</body>
