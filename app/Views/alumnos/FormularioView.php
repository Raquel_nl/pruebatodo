<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="icon" href="<?= base_url('fotos/5.ico') ?>" type="image/x-icon">

        <title>FORMULARIO</title>
    </head>

    <style>

        body {

            background-color:#fdc963;
        }

        input:hover {

            background-color: paleturquoise;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h2 style="text-align: center;">DATOS DEL ALUMNO</h2>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("index.php/formularioController/insertar") ?>">
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-id-card-alt"></i>&nbsp;<label name="nia">NIA</label>
                    <abbr title="Introduce 8 números"><input type="text" class="form-control" name="nia" id="nia" placeholder="Insertar NIA" maxlength="8" ></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Insertar Nombre">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido1">Primer Apellido</label>
                    <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Insertar Primer Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido2">Segundo Apellido</label>
                    <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Insertar Segundo Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-baby"></i>&nbsp;<label name="nacimiento">Fecha de nacimiento</label>
                    <abbr title="Pon tu fecha de nacimiento de la siguiente manera: AÑO-MES-DIA"><input type="text" class="form-control" id="nacimiento" placeholder="Insertar fecha de nacimiento en formato YYYY-MM-DD" maxlength="12" name="nacimiento"></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="far fa-id-card"></i>&nbsp;<label name="nif">NIF o DNI</label>
                    <abbr title="Introduce 8 números y 1 letra en mayúscula"><input type="text" class="form-control" id="nif" name="nif" placeholder="Insertar NIF o DNI" maxlength="9"></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>&nbsp;<label name="email">Dirección de mail</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Insertar tu email">
                </div> <br>
                
                
                
                <!-- set_checkbox -->
                <div class="form-check form-switch" style="width:450px;">
                    <?= form_checkbox('repito','No',false,['class'=>"form-check-input",'id'=>'repito'])?> 
                    <?= form_label('repito','Repites',['class'=>'form-check-label'])?>
                    <i class="fas fa-envelope-square"></i>
                </div> <br> 
                
                 <!-- form_dropdown -->
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>;<label name="genero">Elige tu género</label>
                    <input type="" class="form-control" id="email" name="myradio" value="1">
                    <?= form_dropdown('Género','',false,['class'=>"form-control"])?> 
                    <?= form_label('Elige ','?',['class'=>'control-label'])?>
                </div> <br> 
                
                
                
                
                
                
                
                
                
                <!-- form_fieldeset -->
                <div class="custom-control custom-radio custom-control-inline" style="width:450px;">
                   <?=form_fieldset('Elige tu nivel de ingles')?>
                    
                    <?= form_label('A1','Nivel_ingles',['class'=>'control-label'])?>
                    <input type="radio" id="A1" name="A1" value="A1">

                    <?= form_label('A2','Nivel_ingles',['class'=>'control-label'])?>
                    <input type="radio" id="A2" name="A2" value="A2">
                    
                    <?= form_label('B1','Nivel_ingles',['class'=>'control-label'])?>
                          <input type="radio" id="B1" name="B1" value="B1">
                 
                    <?= form_label('B2','Nivel_ingles',['class'=>'control-label'])?>
                          <input type="radio" id="B2" name="B2" value="B2">
                        
                    <?= form_label('C1','Nivel_ingles',['class'=>'control-label'])?>          
                          <input type="radio" id="C1" name="C1" value="C1">
                     
                    <?= form_label('C2','Nivel_ingles',['class'=>'control-label'])?>      
                          <input type="radio" id="C2" name="C2" value="C2"> 
                              
                    <?=form_fieldset_close()?>
                </div>
                         
                          
                          
                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>


