<?= $header ?>

<!-- <body> -->
<div class="container">
    <table class="table caption-top" style="margin: 0 auto !important">

        <caption>
            TITULO: <?= $title ?>
        </caption>

        <thead>
            <tr>
                <th> id </th>
                <th> Codigo</th>
                <th> Nombre </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($grupos as $grupo) : ?>
                <tr class="hover">
                    <td class="id"> <b> <?= $grupo["id"] ?> </b> </td>
                    <td class="Codigo">
                        <a class="links" href="<?= site_url('gruposController/alumnosGrupos/' . $grupo['codigo']) ?>"> <?= $grupo['codigo'] ?></a>
                    <td class="nombre"> <?= $grupo["nombre"] ?> </td>
                </tr>
            <?php
            endforeach; ?>
        </tbody>

    </table>
</div>

</body>

<?= $footer ?>