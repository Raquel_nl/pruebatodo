<?php

namespace App\Models;

use CodeIgniter\Model;

class AlumnosModel extends Model
{
    protected $table = 'alumnos';
    protected $primaryKey = 'id';
    //
    //    protected $useAutoIncrement = true;
    //
    //    protected $returnType     = 'array';
    //    protected $useSoftDeletes = true;
    //
    protected $allowedFields = [
        'id',
        'NIA',
        'nombre',
        'apellido1',
        'apellido2',
        'fecha_nac',
        'nif',
        'email',
    ];
    //
    //    protected $useTimestamps = false;
    //    protected $createdField  = 'created_at';
    //    protected $updatedField  = 'updated_at';
    //    protected $deletedField  = 'deleted_at';
    //
    //    protected $validationRules    = [];
    //    protected $validationMessages = [];
    //    protected $skipValidation     = false;
}
